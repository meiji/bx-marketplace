<?php
namespace Meiji\BxMarketplace;

/**
 * Класс объявлен для будущих реализаций,
 * в качестве основной контролирующей структуры решений.
 *
 * Class Manager
 *
 * @package Meiji\BxMarketplace
 * @author Artem Iksanov <iksanov@meiji.media>
 * @version 0.0.1
 */
class Manager
{

}
