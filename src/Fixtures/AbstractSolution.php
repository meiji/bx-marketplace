<?php

namespace Meiji\BxMarketplace\Fixtures;

use Meiji\BxMarketplace\AdminMenuProvider;
use CBXVirtualIo;


/**
 * Абстрактный класс реализации поддержки решения Композер-модуля
 * в виде Битрикс-модуля в системе.
 *
 * Class AbstractSolution
 *
 * @package Meiji\BxMarketplace\Fixtures
 * @author Artem Iksanov <iksanov@meiji.media>
 * @version 1.4.3
 */
abstract class AbstractSolution
{
	
	/** @var array */
	protected static $listen;
	
	/** @var array */
	protected static $bindings;
	
	/** @var array */
	protected static $iblockElementProperties;
	
	/** @var array */
	protected static $userTypeProperties;
	
	/** @var string */
	private static $adminDirName = 'admin';
	
	/** @var string */
	private static $propertiesDirName = 'properties';
	
	/** @var string */
	private static $helpersDirName = 'helpers';
	
	/**
	 * @param \Bitrix\Main\EventManager|null $eventManager
	 */
	final public static function register(\Bitrix\Main\EventManager $eventManager = null)
	{
		
		if (!$eventManager) {
			$eventManager = \Bitrix\Main\EventManager::getInstance();
		}
		
		self::includeHelpers($eventManager);
		self::registerUserTypeProperties($eventManager);
		self::registerIBlockProperties($eventManager);
		self::registerAdmin($eventManager);
		self::boot($eventManager);
		self::bind($eventManager);
	}
	
	/**
	 * @param \Bitrix\Main\EventManager $eventManager
	 */
	final private static function boot(\Bitrix\Main\EventManager $eventManager)
	{
		
		if (is_array(static::$listen)) {
			foreach (static::$listen as $event => $handler) {
				$arEvent   = explode('.', $event);
				$arHandler = explode('::', $handler);
				$eventManager->addEventHandler(current($arEvent), next($arEvent),
					[current($arHandler), next($arHandler)]);
			}
		}
	}
	
	/**
	 * @param \Bitrix\Main\EventManager $eventManager
	 */
	final private static function bind(\Bitrix\Main\EventManager $eventManager)
	{
		
		/** @var \Meiji\BxMarketplace\Fixtures\AbstractSolution $bindingSolutionClass */
		
		if (is_array(static::$bindings)) {
			foreach (static::$bindings as $bindingSolutionClass) {
				$bindingSolutionClass::register($eventManager);
			}
		}
	}
	
	/**
	 * @param \Bitrix\Main\EventManager $eventManager
	 */
	final private static function registerAdmin(\Bitrix\Main\EventManager $eventManager)
	{
		
		/** @var \CBXVirtualDirectoryFileSystem $adminDir */
		
		$adminDirPath    = CBXVirtualIo::GetInstance()->CombinePath(self::getSolutionPath(), self::$adminDirName);
		$menuHandlerFunc = 'initAdmin';
		
		$adminDir = CBXVirtualIo::GetInstance()->GetDirectory($adminDirPath);
		
		if ($adminDir->IsExists() && !$adminDir->IsEmpty()) {
			static::$listen['main.OnBuildGlobalMenu'] = static::class . '::' . $menuHandlerFunc;
		}
	}
	
	/**
	 * @param \Bitrix\Main\EventManager $eventManager
	 */
	final private static function registerIBlockProperties(\Bitrix\Main\EventManager $eventManager)
	{
		
		if (is_array(static::$iblockElementProperties)) {
			foreach (static::$iblockElementProperties as $propertyClassName) {
				$eventManager->addEventHandler('iblock', 'OnIBlockPropertyBuildList',
					[$propertyClassName, 'GetUserTypeDescription']);
			}
		}
		
	}
	
	/**
	 * @param \Bitrix\Main\EventManager $eventManager
	 */
	final private static function registerUserTypeProperties(\Bitrix\Main\EventManager $eventManager)
	{
		
		if (is_array(static::$userTypeProperties)) {
			foreach (static::$userTypeProperties as $userPropertyClassName) {
				$eventManager->addEventHandler('main', 'OnUserTypeBuildList',
					[$userPropertyClassName, 'GetUserTypeDescription']);
			}
		}
		
	}
	
	final private static function includeHelpers(\Bitrix\Main\EventManager $eventManager)
	{
		
		$helpersPath    = CBXVirtualIo::GetInstance()->CombinePath(self::getSolutionPath(), self::$helpersDirName);
		$helpersDir = CBXVirtualIo::GetInstance()->GetDirectory($helpersPath);
		
		if ($helpersDir->IsExists() && !$helpersDir->IsEmpty()) {
			$arHelperDirContains = $helpersDir->GetChildren();
			foreach ($arHelperDirContains as $child) {
				if (!$child->IsDirectory()) {
					if ($child->GetExtension() == 'php') {
						include_once $child->GetPathWithName();
					}
				}
			}
		}
	}
	
	/**
	 * @param array $aGlobalMenu
	 * @param array $aModuleMenu
	 */
	final public static function initAdmin(&$aGlobalMenu, &$aModuleMenu)
	{
		
		/** @global \CUser $USER */
		
		global $USER;
		
		/** @todo Add rights checking */
		if ($USER->IsAdmin()) {
			with(new AdminMenuProvider(static::class))->initSolutionMenu($aGlobalMenu, $aModuleMenu);
		}
	}
	
	/**
	 * @return string|null
	 */
	public static function getSolutionPath()
	{
		
		/** @global \Composer\Autoload\ClassLoader $COMPOSER */
		
		global $COMPOSER;
		
		return CBXVirtualIo::GetInstance()
			->GetFile(CBXVirtualIo::GetInstance()->ExtractPathFromPath($COMPOSER->findFile(static::class)))
			->GetPath();
	}
	
	/**
	 * @return string|null
	 */
	public static function getAdminDirName()
	{
		
		return self::$adminDirName;
	}
	
}
